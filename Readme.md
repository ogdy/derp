# **Derp** : A **De**ep learning in C sha**rp**.

**Derp** is a Neural Network generation and training framework written in C#.
It works entirely with builtin arrays and term by term calculations, resulting in very good performances connsidering it is entirely CPU based.

This library was written for my own learning of machine learning topics.